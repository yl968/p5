# Creating a Serverless Rust Microservice with AWS Lambda and DynamoDB

## Step 1: Set Up DynamoDB Table

1. **Navigate to DynamoDB**: Log in to AWS Management Console and go to DynamoDB.
2. **Create Table**: Click "Create table". Enter a name and primary key. 
3. **Save Table**: Click "Create" to finalize the table setup.
![Image](2376c88d46142cbe5f0dc550029311a.png)

## Step 2: Prepare Rust Project

1. **Create Project**: Run `cargo new p5`.
2. **Edit `Cargo.toml`**: Add dependencies.

    ```toml
    [dependencies]
    lambda_runtime = "0.4.1"
    serde = { version = "1.0", features = ["derive"] }
    serde_json = "1.0"
    rusoto_core = "0.47.0"
    rusoto_dynamodb = "0.47.0"
    tokio = { version = "1", features = ["full"] }
    openssl = { version = "0.10", features = ["vendored"] }
    ```

## Step 3: Implement Lambda Function

Write Lambda handler in `main.rs` to include the feature of listing all the tables.
### Functionality

Upon execution, the service sends a `ListTables` request to DynamoDB and returns a JSON response containing the names of all accessible tables. This process requires no input parameters, allowing for easy integration and automation.

```rust
async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    let client = DynamoDbClient::new(Region::UsEast1);
    match client.list_tables(ListTablesInput::default()).await {
        Ok(output) => {
            let tables = output.table_names.unwrap_or_else(|| vec!["No tables found".to_string()]);
            Ok(json!({ "tables": tables }))
        },
        Err(e) => Err(e.into()),
    }
}
```


## Step 4: Compile Rust Code for AWS Lambda

Compile application for `x86_64-unknown-linux-musl` to ensure compatibility with AWS Lambda's execution environment.

```bash
cargo build --release --target x86_64-unknown-linux-musl
```

## Step 5: Deploy Lambda Function

1. **Package Application**: Zip compiled bootstrap binary. In my case, it's located at `p5\target\x86_64-unknown-linux-musl\release`
2. **Create a Lambda Function**: Use AWS Management Console to upload zip file. 

## Step 6: Test
Successful connection to database.
![Image](4bdb43ab110b019aa85c013b0024935.png)