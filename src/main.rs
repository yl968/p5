use lambda_runtime::{handler_fn, Context, Error};
use rusoto_core::Region;
use rusoto_dynamodb::{DynamoDb, DynamoDbClient, ListTablesInput};
use serde_json::{Value, json};

async fn function_handler(event: Value, _: Context) -> Result<Value, Error> {
    let client = DynamoDbClient::new(Region::UsEast1);
    match client.list_tables(ListTablesInput::default()).await {
        Ok(output) => {
            let tables = output.table_names.unwrap_or_else(|| vec!["No tables found".to_string()]);
            Ok(json!({ "tables": tables }))
        },
        Err(e) => {
            Err(e.into())
        },
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(function_handler)).await?;
    Ok(())
}